const api_url = 'https://v2.jokeapi.dev/joke/Programming?blacklistFlags=nsfw,religious,political,racist,sexist,explicit&type=single'

async function getJoke() {
    const response = await fetch(api_url);
    const data = await response.json();
    const  {joke} = data;
    console.log(joke);

    document.getElementById('joke').textContent = joke;
}

getJoke();

function refresh(){
    window.location.reload("Refresh")
}