
function addToList() {

    var value = document.getElementById("inputText").value;

    if (value === "") {
      alert('Lisääksesi tavaran kirjoita se tähän tekstikenttään')
    }
    else {

    var li = document.createElement("li");
    var textNode = document.createTextNode(value);

    li.appendChild(textNode);
    document.getElementById("list").appendChild(li);
    li.className = "item";
    };
  };